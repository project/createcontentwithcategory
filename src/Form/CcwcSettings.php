<?php

namespace Drupal\createcontentwithcategory\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Create Content with Category module.
 */
class CcwcSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['createcontentwithcategory.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'createcontentwithcategory_settings';
  }

  /**
   * Gather up all content types with category fields we could pre-fill.
   */
  public function gatherNodeTypesWithTermReference() {
    $node_types_terms = [];
    $node_types = node_type_get_names();
    foreach ($node_types as $type => $type_label) {
      $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $type);
      // Surely there's a better way to get all the taxonomy term reference
      // fields but until we find that way, we loop through field definitions.
      $term_fields = [];
      foreach ($fields as $field => $dfn) {
        if ($dfn->getType() === 'entity_reference' && $dfn->getSettings()['target_type'] === 'taxonomy_term') {
          $vocab_names = $dfn->getSettings()['handler_settings']['target_bundles'];
          // @TODO get vocab labels and link to vocabs.
          $term_fields[$field] = $dfn->getLabel() . ' (' . implode(',', $vocab_names) . ')';
        }
      }
      if ($term_fields) {
        $node_types_terms[$type] = [
          'label' => $type_label,
          'term_fields' => $term_fields,
        ];
      }
    }
    return $node_types_terms;

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('createcontentwithcategory.settings');

    // Overall Configuration.
    /*
    $form['configuration'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Overall configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    ];
    $form['configuration']['link_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create blocks for all terms on '),
      '#description' => $this->t('This is probably not a good idea and is not currently implemented).'),
      '#default_value' => $config->get('link_all'),
    ];
    */

    $form['target_nodes_fields'] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => $this->t('Which Content Types and Categories to Make Links For'),
      '#description' => $this->t("From the available content types with taxonomy term reference fields, select the types and categories for which you would like to have quick create links with categories prepopulated."),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#states' => [
        // Hide this fieldset when all forms are protected.
        'invisible' => [
          'input[name="link_all"]' => ['checked' => TRUE],
        ],
      ],
    ];
    
    $node_types_terms = $this->gatherNodeTypesWithTermReference();

    if (!empty($node_types_terms)) {
      foreach ($node_types_terms as $type => $info) {
        $form['target_nodes_fields'][$type] = [
          '#type' => 'fieldset',
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
          '#title' => $info['label'],
        ];

        foreach ($info['term_fields'] as $field => $label) {
          $id = $type . '__' . $field;
          $form['target_nodes_fields'][$type][$id] = [
            '#type' => 'checkbox',
            '#title' => $label,
            '#default_value' => $this->isTargetNodeField($id),
          ];
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $targets_array = $form_state->getValue('target_nodes_fields');
    if (is_array($targets_array)) {
      $target_nodes_fields = array_merge(...array_values($targets_array));
      // Simplify the output to save to:
      // target_nodes_fields:
      //   - event__field_event_type
      //   - press_release__field_categories
      // Instead of:
      //   event__field_event_type: 1
      //   page__field_page_type: 0
      // etc.
      $target_nodes_fields = array_keys(array_filter($target_nodes_fields));
    }
    else {
      $target_nodes_fields = [];
    }
    $this->config('createcontentwithcategory.settings')
      ->set('target_nodes_fields', $target_nodes_fields)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Return true if the ID is in the array of active node-field combos.
   */
  public function isTargetNodeField($id) {
    $config = $this->config('createcontentwithcategory.settings');
    $target_nodes_fields = $config->get('target_nodes_fields');
    if (is_array($target_nodes_fields) && in_array($id, $target_nodes_fields)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

}

<?php

namespace Drupal\createcontentwithcategory\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\createcontentwithcategory\Ccwc;


/**
 * Provides a block.
 *
 * @Block(
 *   id = "createcontentwithcategory_block",
 *   admin_label = @Translation("Create Content with Category links block"),
 *   deriver = "Drupal\createcontentwithcategory\Plugin\Derivative\CreateContentWithCategoryBlock"
 * )
 */

class CreateContentWithCategoryBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $ccwc = new Ccwc($this->getDerivativeId());
    return $ccwc->build();
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $ccwc = new Ccwc($this->getDerivativeId());
    $ccwc->permission();
    return AccessResult::allowedIfHasPermission($account, $permission);
  }

}

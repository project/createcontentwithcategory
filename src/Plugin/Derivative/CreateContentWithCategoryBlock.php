<?php

namespace Drupal\createcontentwithcategory\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class CreateContentWithCategoryBlock extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $target_nodes_fields = createcontentwithcategory_target_nodes_fields();
    foreach ($target_nodes_fields as $id => $ccwc) {
      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['admin_label'] = $ccwc->label();
    }
  
    return $this->derivatives;
  }
}
